function problem5(inventory,allCarYears){
    const allOldCarYear=[];
    for (let i=0;i<allCarYears.length;i++){
        if (allCarYears[i]<2000){
            let oldCarYear=allCarYears[i];
            allOldCarYear.push(oldCarYear);
        }
    }
    return allOldCarYear;
}
module.exports=problem5;