function problem6(inventory){
    const allBMWAndAudiCars =[];
    for (let i=0;i<inventory.length;i++){
        let carObject= inventory[i];
        if(carObject.car_make==="BMW" || carObject.car_make==="Audi"){
            let BMWAndAudiCar=inventory[i];
            allBMWAndAudiCars.push(BMWAndAudiCar);
        }
    }
    
    return allBMWAndAudiCars;
}
    
module.exports=problem6;    