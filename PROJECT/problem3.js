function problem3(inventory){
    const allCarModel=[];
    
    for (let i=0 ;i < inventory.length;i++){
        let carModel=inventory[i].car_model;
        allCarModel.push(carModel);
    }

    for (let i=0 ;i<allCarModel.length;i++){
        for (let j=0 ;j<allCarModel.length-1;j++){
            if (allCarModel[j].toLowerCase() > allCarModel[j+1].toLowerCase()){
                let swapElement=allCarModel[j];
                allCarModel[j]=allCarModel[j+1];
                allCarModel[j+1]=swapElement;
            }
        }
    }
    
   return allCarModel;
}
module.exports=problem3;